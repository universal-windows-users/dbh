from .detector import detector_blueprint

__all__ = ["detector_blueprint"]
__version__ = "0.0.1"
