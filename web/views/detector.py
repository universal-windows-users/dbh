import base64
import binascii
import io

import flask
import magic
import requests
import werkzeug.exceptions

detector_blueprint = flask.Blueprint(
	"detector",
	__name__
)


@detector_blueprint.route("/", methods=["GET", "POST"])
def detect():
	if (
		flask.request.method == "POST" and
		"image" in flask.request.form and
		"visualize" in flask.request.form
	):
		visualize = (flask.request.form["visualize"] == "true")

		try:
			decoded_image = base64.b64decode(flask.request.form["image"], validate=True)
		except (ValueError, binascii.Error):
			raise werkzeug.exceptions.BadRequest

		image_buffer = io.BytesIO(decoded_image)

		image_type = magic.from_buffer(
			image_buffer.read(2048),
			mime=True
		)

		image_buffer.seek(0)

		if image_type not in (
			"image/png",
			"image/jpeg",
			"image/tiff",
			"image/bmp"
		):
			raise werkzeug.exceptions.BadRequest

		response = requests.post(
			f"{flask.current_app.config['SERVER_URL']}upload?image={0 if visualize else 1}",
			files={
				"file": (f"file.{image_type.split('/')[1]}", image_buffer)
			},
			timeout=120
		)

		if not response.ok:
			return flask.render_template(
				"detect_error.html",
				error=None
			), response.status_code

		if not visualize:
			return flask.render_template(
				"detect_result_noninteractive.html",
				image=base64.b64encode(response.raw)
			)
		else:
			response = response.json()
			
			return flask.render_template(
				"detect_result.html",
				image=flask.request.form["image"],
				cell_data=response["data"],
				width=response["shape"][1],
				height=response["shape"][0]
			)

	return flask.render_template("detect_start.html")
