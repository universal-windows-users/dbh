"""Medical cell detection."""

from __future__ import annotations

import json
import logging
import os
import typing

import flask
import flask_babel

__all__ = [
	"create_app",
	"get_config"
]
__version__ = "0.0.1"

logging.basicConfig(
	format="[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
	level=getattr(
		logging,
		os.environ.get(
			"LOGGING_LEVEL",
			"DEBUG"
		)
	)
)


def get_config(
	skip_context_check: bool = False,
	config_location_envvar: str = "CONFIG_LOCATION",
	fallback_name: str = "config.json"
) -> typing.Dict[str, typing.Any]:
	"""Gets the current JSON config for this service.

	If this function is run in a Flask context and ``skip_context_check`` is
	:data:`True`, :attr:`flask.current_app.config` is returned. Otherwise, an
	attempt is made to use the ``config_location_envvar`` environment variable
	for the config file. If it exists, the location it points to will be used.
	Otherwise, the current working directory's ``fallback_name`` is used.

	:param config_location_envvar: The environment variable to use in order to
		find the custom config file. ``CONFIG_LOCATION`` by default.
	:param fallback_name: The filename to use when the environment variable is
		not set. Defaults to ``config.json``.

	.. warning::
		It's assumed that if the environment variable is set, the location is
		valid and the file exists. If it is not set, ``config.json`` must exist.

	:returns: The config, in a Python dictionary.
	"""

	if not skip_context_check and flask.has_app_context():
		return flask.current_app.config

	with open(
		os.environ.get(
			config_location_envvar,
			os.path.join(os.getcwd(), fallback_name)
		),
		"r",
		encoding="utf-8"
	) as config_file:
		return json.load(config_file)


def create_app() -> flask.Flask:
	r"""Creates a :class:`Flask <flask.Flask>` app with settings already applied.

	Loads the config from the file located where the ``CONFIG_LOCATION``
	environment variable describes, or ``$current_working_directory/config.json``
	if it's not set.

	Registers all blueprints in :mod:`.views`.

	:returns: The finished app.
	"""

	app = flask.Flask(__name__)

	with app.app_context():
		app.logger.info("Setting up app")

		app.logger.debug("Loading config file")

		app.config.update(get_config(skip_context_check=True))

		app.babel = flask_babel.Babel(app)

		from .views import detector_blueprint

		for blueprint in (detector_blueprint,):
			app.logger.debug("Registering blueprint: %s", blueprint)

			app.register_blueprint(blueprint)

	app.logger.info("App setup finished")

	return app
