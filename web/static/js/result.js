// size of the tangent
var t = 1 / 5;

function controlPoints(p) {
	// given the points array p calculate the control points for the cubic Bezier curves

	var pc = [];
	
	for (var i = 1; i < p.length - 1; i++) {
		var dx = p[i - 1].x - p[i + 1].x; // difference x
		var dy = p[i - 1].y - p[i + 1].y; // difference y
		// the first control point
		var x1 = p[i].x - dx * t;
		var y1 = p[i].y - dy * t;
		var o1 = {
			x: x1,
			y: y1
		};

		// the second control point
		var x2 = p[i].x + dx * t;
		var y2 = p[i].y + dy * t;
		var o2 = {
			x: x2,
			y: y2
		};

		// building the control points array
		pc[i] = [];
		pc[i].push(o1);
		pc[i].push(o2);
	}
	
	return pc;
}

function drawCurve(p) {
	var pc = controlPoints(p); // the control points array
	
	let d =`M${p[0].x},${p[0].y}
	Q${pc[1][1].x},${pc[1][1].y}, ${p[1].x},${p[1].y}`;
	
	if (p.length > 2) {
		// central curves are cubic Bezier
		for (var i = 1; i < p.length - 2; i++) {
			d += `C${pc[i][0].x}, ${pc[i][0].y}, ${pc[i + 1][1].x}, ${pc[i + 1][1].y}, ${p[i + 1].x},${p[i + 1].y}`;
		}
		
		// the first & the last curve are quadratic Bezier
		var n = p.length - 1;
		d+= `Q${pc[n - 1][0].x}, ${pc[n - 1][0].y}, ${p[n].x}, ${p[n].y}`;
	}
	
	return d
}

function generateSvg(pointSet) {
	window.objects = {};
	
	window.coreAreas = [];
	window.shellAreas = [];
	
	// https://stackoverflow.com/a/62857298
	// Thanks to enxaneta!
	for (const points of pointSet) {
		const shellPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
		shellPath.id = points.id.toString() + "-shell";
		
		window.objects[points.id.toString() + "-shell"] = {
			type: "shell",
			relatesTo: points.id.toString() + "-core"
		}
		
		const corePath = document.createElementNS("http://www.w3.org/2000/svg", "path");
		corePath.id = points.id.toString() + "-core";
		
		window.objects[points.id.toString() + "-core"] = {
			type: "core",
			relatesTo: points.id.toString() + "-shell"
		}
		
		let shellPoints = [];
		let corePoints = [];

		let currentShellArea = 0;
		let currentCoreArea = 0;

		for (const point of points.core_points) {
			corePoints.push({
				x: point[0][0],
				y: point[0][1] // ?
			});
		}
		
		window.coreAreas.push(points.core_area);
		currentCoreArea += points.core_area;
		
		for (const pointSet of points.shell) {
			for (const point of pointSet.container_points) {
				shellPoints.push({
					x: point[0][0],
					y: point[0][1] // ?
				});
				
			}
			
			window.shellAreas.push(pointSet.container_area);
			currentShellArea += pointSet.container_area;
		}
		
		shellPath.setAttribute("d", drawCurve(shellPoints));
		shellPath.dataset.area = currentShellArea;
		shellPath.style.fill = "rgba(255, 0, 0, 0)";
		
		corePath.setAttribute("d", drawCurve(corePoints));
		corePath.dataset.area = currentCoreArea;
		corePath.style.fill = "rgba(0, 255, 0, 0)";
		
		document.getElementById("result-svg").appendChild(shellPath);
		document.getElementById("result-svg").appendChild(corePath);
	}
	
	loadResultInterface(window.objects);
}

function loadResultInterface(objects) {
	document.getElementById("result-svg").addEventListener(
		"mousemove",
		function (event) {
			if (event.target.tagName === "svg") {
				for (const element of document.getElementsByClassName("information-window")) {
					element.remove();
				}
				
				for (const id of Object.keys(window.objects)) {
					document.getElementById(id).style.fill = "rgba(255, 255, 255, 0)";
				}
			}
		}
	);
	
	document.getElementById("show-core").addEventListener(
		"change",
		function (event) {
			for (const id of Object.keys(window.objects)) {
				if (!id.endsWith("-core")) {
					continue;
				}
				
				if (event.target.checked) {
					document.getElementById(id).style.stroke = "rgba(0, 255, 0, 1)";
					document.getElementById(id).style.strokeWidth = "3px";
				} else {
					document.getElementById(id).style.stroke = "rgba(0, 255, 0, 0)";
				}
			}
		}
	);
	
	document.getElementById("show-shell").addEventListener(
		"change",
		function (event) {
			for (const id of Object.keys(window.objects)) {
				if (!id.endsWith("-shell")) {
					continue;
				}
				
				if (event.target.checked) {
					document.getElementById(id).style.stroke = "rgba(255, 0, 0, 1)";
					document.getElementById(id).style.strokeWidth = "3px";
				} else {
					document.getElementById(id).style.stroke = "rgba(255, 0, 0, 0)";
				}
			}
		}
	);
	
	for (const id of Object.keys(objects)) {
		document.getElementById(id).addEventListener(
			"mousemove",
			function (event) {
				// Delete old windows
				for (const element of document.getElementsByClassName("information-window")) {
					element.remove();
				}
				
				for (const id of Object.keys(objects)) {
					document.getElementById(id).style.fill = "rgba(255, 255, 255, 0)";
				}
				
				const informationWindow = document.createElement("section");
				
				switch (objects[id].type) {
					case "core":
						informationWindow.innerHTML = (
							"Core " + event.target.id.split("-")[0]
							+ "<br>"
							+ `<small>Area: ${event.target.dataset.area}</small>`
						);
						break;
					case "shell":
						informationWindow.innerHTML = (
							"Shell " + event.target.id.split("-")[0]
							+ "<br>"
							+ `<small>Area: ${event.target.dataset.area}</small>`
						);
						break;
				}
				
				informationWindow.classList.add("information-window");
				informationWindow.style.left = `${event.pageX + 5}px`;
				informationWindow.style.top = `${event.pageY + 5}px`;
				
				informationWindow.addEventListener(
					"hover",
					function (event) {
						event.target.delete();
					}
				);
				
				if (event.target.id.endsWith("core")) {
					event.target.style.fill = "rgba(0, 255, 0, 0.5)";
				} else if (event.target.id.endsWith("shell")) {
					event.target.style.fill = "rgba(255, 0, 0, 0.5)";
				}
				
				if (objects[id].relatesTo !== undefined) {
					const relatedElement = document.getElementById(objects[id].relatesTo);
					
					if (relatedElement.id.endsWith("-core")) {
						relatedElement.style.fill = "rgba(0, 255, 0, 0.5)";
					} else if (relatedElement.id.endsWith("-shell")) {
						relatedElement.style.fill = "rgba(255, 0, 0, 0.5)";
					}
				}
				
				document.body.appendChild(informationWindow);
			}
		);
	}
}

window.addEventListener(
	"DOMContentLoaded",
	function (event) {
		document.getElementById("download-json").addEventListener(
			"click",
			function(event) {
				// https://stackoverflow.com/a/35251739
				// Thanks to ahuff44!
				
				const blob = new Blob([
					JSON.stringify({
						"cellData": window.cellData,
						"objectTypes": window.objects
					})
				], {type: "application/json"});
				
				var dlink = document.createElement("a");
				dlink.download = name;
				dlink.href = window.URL.createObjectURL(blob);
				dlink.onclick = function(e) {
					// revokeObjectURL needs a delay to work properly
					var that = this;
					setTimeout(function() {
						window.URL.revokeObjectURL(that.href);
					}, 1500);
				};

				dlink.click();
				dlink.remove();
			}
		);
	}
);
