window.addEventListener(
	"DOMContentLoaded",
	function (event) {
		document.getElementById("image").addEventListener(
			"change",
			function (event) {
				// https://base64.guru/developers/javascript/examples/encode-form-file
				// Thanks to Base64 guru!
				const reader = new FileReader();

				reader.addEventListener(
					"loadend",
					function () {
						// Since it contains the Data URI, we should remove the prefix and keep only Base64 string
						const encodedContent = reader.result.replace(/^data:.+;base64,/, '');
						
						document.getElementById("image-button").style.display = "none";
						document.getElementById("visualize-wrapper").style.display = "none";
						document.getElementById("image").style.display = "none";
						document.getElementById("image").disabled = true;
						document.getElementById("bottom-text").innerHTML = "Loading...";
						
						const formData = new FormData();
						
						formData.append("image", encodedContent);
						formData.append("visualize", document.getElementById("visualize").checked.toString());
						
						// https://stackoverflow.com/a/50101022
						// Thanks to Endless!
						const controller = new AbortController();
						const timeoutId = setTimeout(() => controller.abort(), 120000);
						
						fetch("/", {method: "POST", body: formData, signal: controller.signal}).
						then(function (response) {
							if (response.status != 200) {
								throw new Error(`Velky spatny: ${response.status}`);
							}
							
							clearTimeout(timeoutId)
							
							const replacementHTML = document.open("text/html", "replace");
							return response.text().then(
								function(text) {
									replacementHTML.write(text);
									replacementHTML.close();
								}
							);
						});
					}
				);

				reader.readAsDataURL(event.target.files[0]);
			}
		);
	}
);
